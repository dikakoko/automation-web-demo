Before do
  page.driver.browser.manage.window.maximize
end

After do |scenario|
  if scenario.failed?
    screenshot_path = "screenshot/error/#{scenario.name.gsub(" ", "_")}.png"
    Capybara.page.save_screenshot(screenshot_path, full: true)
    attach(screenshot_path, 'image/png')
  end
end
