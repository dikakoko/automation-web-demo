Feature: Swag Labs login feature

  @Login @SuccessLogin
  Scenario: User success login to swag labs
    Given user visit swag labs
    When user input username "STANDARD_USERNAME"
    And user input password "STANDARD_PASSWORD"
    And user click login button
    Then user see product page

  @Login @SuccessLogin
  Scenario: User login using wrong password
    Given user visit swag labs
    When user input username "nanananan"
    And user input password "nanananan"
    And user click login button
    Then user see login alert "Epic sadface: Username and password do not match any user in this service"