class ProductPage
  include Capybara::DSL

  def is_product_page_title
    find(".title").text
  end
end