class LoginPage
  include Capybara::DSL

  def input_username(username)
    find("#user-name").send_keys(Helper.get_env(username))
  end

  def input_password(password)
    find("#password").send_keys(Helper.get_env(password))
  end

  def click_button_login
    find("#login-button").click
  end

  def alert_message(message)
    find(:xpath, "//h3[text()='#{message}']")
  end
end
