login_page = LoginPage.new

Given(/^user visit swag labs$/) do
  visit(ENV["URL"])
end

When(/^user input username "([^"]*)"$/) do |username|
  login_page.input_username(username)
end

And(/^user input password "([^"]*)"$/) do |password|
  login_page.input_password(password)
end

And(/^user click login button$/) do
  login_page.click_button_login
end

Then(/^user see login alert "([^"]*)"$/) do |message|
  expect(login_page.alert_message(message)).to be_visible
end