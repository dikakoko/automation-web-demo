class Helper
  class << self
    def get_env(key)
      ENV[key] || key
    end
  end
end